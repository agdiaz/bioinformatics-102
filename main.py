import os

from flask import Flask, request, jsonify
import transcriptb2b as b2b


app = Flask(__name__)

@app.route("/", methods=['POST'])
def dna2rna():
  dna_sequence = request.json['dnaSequence']
  rna_sequence = b2b.transcript(dna_sequence)

  return jsonify(rnaSequence=rna_sequence, dnaSequence=dna_sequence)


if __name__ == "__main__":
  app.run(debug=False, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
