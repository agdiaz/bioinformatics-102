# This is an example file to test the pypi package
from transcriptb2b import transcript, reverseT

my_dna_sequence = 'AAAGTGGAAACCCGGTAACCCGGTGGAAAAAAACCCGGTGGAAACCCGGTAAAAACCCGTGGAAACCCGGTGGTGGGG'

my_rna_sequence = transcript(my_dna_sequence)

my_reversed_dna_sequence = reverseT(my_rna_sequence)

assert my_dna_sequence == my_reversed_dna_sequence, "Houston we've got a transcription problem"
